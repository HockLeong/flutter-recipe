import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_recipe/constants/constant.dart';
import 'package:flutter_recipe/model/ingredient_model.dart';
import 'package:flutter_recipe/model/instruction_model.dart';
import 'package:flutter_recipe/model/recipe_model.dart';
import 'package:flutter_recipe/vm/recipe_details_view_model.dart';
import 'package:flutter_recipe/vm/recipe_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';

late RecipeModel mockRecipeItem;
late Uint8List photoBytes;

Future<void> initialiseHive() async {
  var path = Directory.current.path;
  Hive
    ..init(path)
    ..registerAdapter(RecipeModelAdapter())..registerAdapter(
      IngredientModelAdapter())..registerAdapter(InstructionModelAdapter());

  // Always starts from a clean box
  Hive.deleteBoxFromDisk(Constant.RECIPE_BOX_NAME);
  Hive.deleteBoxFromDisk(Constant.INGREDIENT_BOX_NAME);
  Hive.deleteBoxFromDisk(Constant.INSTRUCTION_BOX_NAME);
}

Future <void> initMockData() async {
  ByteData byteData = await rootBundle.load('assets/images/recipe.jpeg');
  photoBytes = byteData.buffer.asUint8List();

  mockRecipeItem = RecipeModel(
    name: "Recipe One",
    recipeType: "Halal",
    photoBytes: photoBytes,
    ingredients: [
      IngredientModel(
        count: 20,
        unit: "g",
        name: "Chili",
      )
    ],
    instructions: [
      InstructionModel(
        step: 1,
        description: "Cut the chili",
      )
    ],
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initialiseHive();
  await initMockData();

  RecipeViewModel recipeViewModel = Get.put(RecipeViewModel());
  RecipeDetailsViewModel recipeDetailsViewModel =
  Get.put(RecipeDetailsViewModel());

  group('Given Recipe List Page Loads', () {
    test('Page should load a list of recipe from Hive', () async {
      await recipeViewModel.getRecipeList();
      expect(recipeViewModel.recipeListStream.length, 0);
    });
  });

  test('When user create a new recipe, a record should be saved', () async {
    recipeDetailsViewModel.nameStream.value = mockRecipeItem.name ?? '';
    recipeDetailsViewModel.recipeTypeStream.value =
        mockRecipeItem.recipeType ?? '';
    recipeDetailsViewModel.photoBytesStream.value =
        mockRecipeItem.photoBytes ?? [];
    recipeDetailsViewModel.instructionModelListStream
        .assignAll(mockRecipeItem.instructions ?? []);
    recipeDetailsViewModel.ingredientModelListStream
        .assignAll(mockRecipeItem.ingredients ?? []);
    await recipeDetailsViewModel.submit();

    expect((recipeViewModel.recipeListStream.length), 1);
  });

  test(
      'After a record insertion, check recipe item whether insert correctly or not',
          () async {
        await recipeViewModel.getRecipeList();

        expect(recipeViewModel.recipeListStream[0].name, "Recipe One");
        expect(recipeViewModel.recipeListStream[0].recipeType, "Halal");
        expect(
          recipeViewModel.recipeListStream[0].ingredients![0].count,
          20,
        );
        expect(
          recipeViewModel.recipeListStream[0].instructions![0].description,
          "Cut the chili",
        );
      });

  test('User update recipe item and match the outcome', () async {
    // Update first to-do item
    recipeDetailsViewModel.nameStream.value = "Recipe Two";
    recipeDetailsViewModel.recipeTypeStream.value = "Non-Halal";
    recipeDetailsViewModel.ingredientModelListStream.value = [
      IngredientModel(
        count: 100,
        unit: "g",
        name: "Potato",
      )
    ];
    recipeDetailsViewModel.instructionModelListStream.value = [
      InstructionModel(
        step: 1,
        description: "Cut the potato",
      )
    ];
    await recipeDetailsViewModel.submit(index: 0);

    RecipeModel updateItem =
        await recipeDetailsViewModel.getRecipeByIndex(0) ?? RecipeModel();
    expect(updateItem.name, "Recipe Two");
    expect(updateItem.recipeType, "Non-Halal");
    expect(updateItem.ingredients![0].count, 100);
    expect(updateItem.instructions![0].description, "Cut the potato");
  });
}
