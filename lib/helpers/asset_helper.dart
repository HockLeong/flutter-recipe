import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/constant_model.dart';

class AssetHelper {
  static Future<List<String>> getRecipeTypeList() async {
    // Read recipe types from json
    String recipeTypeJson =
        await rootBundle.loadString("assets/json/recipe_type.json");
    return (json.decode(recipeTypeJson) as List).cast<String>();
  }

  static Future<List<ConstantModel>> getUnitList() async {
    // Read unit types from json file
    String unitJson = await rootBundle.loadString("assets/json/unit_type.json");
    return (json.decode(unitJson) as List)
        .map((i) => ConstantModel.fromJson(i))
        .toList();
  }
}
