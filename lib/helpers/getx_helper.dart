import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetXHelper {
  static void showErrorMessage(String message) {
    // Prevent SnackBar show twice
    if (Get.isSnackbarOpen) return;

    Get.snackbar(
      "Error",
      message,
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.yellow.withOpacity(0.6),
    );
  }
}
