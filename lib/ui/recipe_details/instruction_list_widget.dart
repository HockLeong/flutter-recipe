import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../model/instruction_model.dart';
import '../../vm/recipe_details_view_model.dart';
import '../../widgets/recipe_instruction_widget.dart';

class InstructionListWidget extends StatefulWidget {
  final Function(InstructionModel instructionModel)? onInstructionAdded;
  final Function(int index, InstructionModel instructionModel)?
      onInstructionUpdated;
  final Function(int index, InstructionModel instructionModel)?
      onInstructionRemoved;

  const InstructionListWidget({
    super.key,
    this.onInstructionAdded,
    this.onInstructionUpdated,
    this.onInstructionRemoved,
  });

  @override
  State<StatefulWidget> createState() => _InstructionListWidgetState();
}

class _InstructionListWidgetState extends State<InstructionListWidget> {
  late List<InstructionModel> _instructionModelList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        GetX<RecipeDetailsViewModel>(builder: (snapshot) {
          _instructionModelList = snapshot.instructionModelListStream;

          return _instructionModelList.isNotEmpty
              ? _getInstructionListWidget(_instructionModelList)
              : const SizedBox.shrink();
        }),
        _addInstructionWidget(),
      ],
    );
  }

  Widget _getInstructionListWidget(List<InstructionModel> instructionList) {
    return Wrap(
        runSpacing: 16.0,
        children: instructionList.map((element) {
          int index = instructionList.indexOf(element);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _headerWidget(index, element),
              _bodyWidget(instructionList, element),
            ],
          );
        }).toList());
  }

  Widget _headerWidget(int index, InstructionModel item) {
    return Container(
      padding: const EdgeInsets.only(
        bottom: 16.0,
      ),
      child: Align(
        alignment: Alignment.topRight,
        child: Wrap(
          spacing: 8.0,
          crossAxisAlignment: WrapCrossAlignment.end,
          children: [
            InkWell(
              onTap: () {
                Get.bottomSheet(
                  RecipeInstructionWidget(
                    instructionModel: item,
                    onCallback: (instructionModel) {
                      widget.onInstructionUpdated
                          ?.call(index, instructionModel);
                    },
                  ),
                  backgroundColor: AppColors.themeBlue,
                );
              },
              child: Image.asset(
                'assets/images/icons/edit_gray.png',
                width: 20.0,
                height: 20.0,
              ),
            ),
            InkWell(
              onTap: () => widget.onInstructionRemoved?.call(index, item),
              child: Image.asset(
                'assets/images/icons/delete.png',
                width: 20.0,
                height: 20.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bodyWidget(
    List<InstructionModel> instructionList,
    InstructionModel item,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Step ${item.step}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16.0),
        Text(
          item.description ?? '',
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
        Visibility(
          visible: instructionList.last != item,
          child: Column(
            children: [
              const SizedBox(height: 16.0),
              _dividerWidget(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _addInstructionWidget() {
    return Center(
      child: InkWell(
        onTap: () {
          Get.bottomSheet(
            RecipeInstructionWidget(
              instructionModel: InstructionModel().copyWith(
                step: _instructionModelList.length + 1,
              ),
              onCallback: (instructionModel) {
                widget.onInstructionAdded?.call(instructionModel);
              },
            ),
            backgroundColor: AppColors.themeBlue,
          );
        },
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: const Text(
            'Add Instructions',
            style: TextStyle(
              decoration: TextDecoration.underline,
              fontSize: 12,
            ),
          ),
        ),
      ),
    );
  }

  Widget _dividerWidget() => Container(
        padding: const EdgeInsets.only(top: 16.0),
        child: const Divider(
          height: 2.0,
          color: AppColors.primaryLightTextColor,
          thickness: 2.0,
        ),
      );
}
