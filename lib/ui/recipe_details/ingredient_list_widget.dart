import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../model/constant_model.dart';
import '../../model/ingredient_model.dart';
import '../../vm/recipe_details_view_model.dart';
import '../../widgets/recipe_ingredient_widget.dart';

class IngredientListWidget extends StatefulWidget {
  final Function(IngredientModel instructionModel)? onIngredientAdded;
  final Function(int index, IngredientModel instructionModel)?
      onIngredientUpdated;
  final Function(int index, IngredientModel instructionModel)?
      onIngredientRemoved;

  const IngredientListWidget({
    super.key,
    this.onIngredientAdded,
    this.onIngredientUpdated,
    this.onIngredientRemoved,
  });

  @override
  State<IngredientListWidget> createState() => _IngredientListWidgetState();
}

class _IngredientListWidgetState extends State<IngredientListWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GetX<RecipeDetailsViewModel>(builder: (snapshot) {
          List<IngredientModel> ingredientModelList =
              snapshot.ingredientModelListStream;

          return ingredientModelList.isNotEmpty
              ? _ingredientListWidget(ingredientModelList)
              : const SizedBox.shrink();
        }),
        _addIngredientWidget(),
      ],
    );
  }

  Widget _ingredientListWidget(List<IngredientModel> ingredientModelList) {
    return Wrap(
      runSpacing: 16.0,
      children: [
        ...ingredientModelList.map((element) {
          int index = ingredientModelList.indexOf(element);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Wrap(
                  spacing: 8.0,
                  children: [
                    GetX<RecipeDetailsViewModel>(
                      builder: (snapshot) {
                        List<ConstantModel>? constantModelList =
                            snapshot.unitListStream;
                        bool hasValue = constantModelList.isNotEmpty;
                        return hasValue
                            ? InkWell(
                                onTap: () {
                                  Get.bottomSheet(
                                    RecipeIngredientWidget(
                                      constantModelList,
                                      ingredientModel: element,
                                      onCallBack: (ingredientModel) {
                                        widget.onIngredientUpdated
                                            ?.call(index, ingredientModel);
                                      },
                                    ),
                                    backgroundColor: AppColors.themeBlue,
                                  );
                                },
                                child: Image.asset(
                                  'assets/images/icons/edit_gray.png',
                                  width: 20.0,
                                  height: 20.0,
                                ),
                              )
                            : const SizedBox.shrink();
                      },
                    ),
                    InkWell(
                      onTap: () {
                        widget.onIngredientRemoved?.call(index, element);
                      },
                      child: Image.asset(
                        'assets/images/icons/delete.png',
                        width: 20.0,
                        height: 20.0,
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Text(
                    element.name!,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 16.0),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    '${(element.count?.toString())}',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 4.0),
                  Text(
                    element.unit ?? '',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              index == ingredientModelList.length - 1
                  ? const SizedBox.shrink()
                  : _getDividerWidget()
            ],
          );
        }).toList()
      ],
    );
  }

  Widget _addIngredientWidget() {
    return Center(
      child: GetX<RecipeDetailsViewModel>(
        builder: (snapshot) {
          List<ConstantModel> constantModelList = snapshot.unitListStream;
          return constantModelList.isNotEmpty
              ? InkWell(
                  onTap: () {
                    Get.bottomSheet(
                      RecipeIngredientWidget(
                        constantModelList,
                        onCallBack: (ingredientModel) {
                          widget.onIngredientAdded?.call(ingredientModel);
                        },
                      ),
                      backgroundColor: AppColors.themeBlue,
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.all(16.0),
                    child: const Text(
                      'Add Ingredients',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                      ),
                    ),
                  ),
                )
              : const SizedBox.shrink();
        },
      ),
    );
  }

  Widget _getDividerWidget() => Container(
        padding: const EdgeInsets.only(top: 16.0),
        child: const Divider(
          height: 2.0,
          color: AppColors.primaryLightTextColor,
          thickness: 2.0,
        ),
      );
}
