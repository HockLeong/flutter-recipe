import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_recipe/helpers/getx_helper.dart';
import 'package:flutter_recipe/model/instruction_model.dart';
import 'package:flutter_recipe/ui/recipe_details/instruction_list_widget.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../model/base_model.dart';
import '../../model/ingredient_model.dart';
import '../../vm/recipe_details_view_model.dart';
import 'ingredient_list_widget.dart';

class RecipeDetailsScreen extends StatefulWidget {
  const RecipeDetailsScreen({
    Key? key,
    this.index,
  }) : super(key: key);

  final int? index;

  @override
  State<RecipeDetailsScreen> createState() => _RecipeDetailsScreenState();
}

class _RecipeDetailsScreenState extends State<RecipeDetailsScreen> {
  late RecipeDetailsViewModel _viewModel;
  late bool isCreateForm;

  @override
  void initState() {
    super.initState();
    _init();
  }

  void _init() async {
    isCreateForm = widget.index == null;

    // Instantiate view model
    _viewModel = Get.put(RecipeDetailsViewModel());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _viewModel.init(index: widget.index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
          title: Text(
            isCreateForm ? "Create Recipe" : "Recipe Details",
            style: const TextStyle(color: Colors.black),
          ),
          actions: [
            _deleteButton(),
          ]),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _nameWidget(),
                  const SizedBox(height: 16.0),
                  _recipeTypeWidget(),
                  const SizedBox(height: 16.0),
                  _recipeImageWidget(),
                  const SizedBox(height: 16.0),
                  _ingredientsWidget(),
                  const SizedBox(height: 16.0),
                  _instructionWidget(),
                  const SizedBox(height: 32.0),
                ],
              ),
            ),
          ),
          _submitButton(),
        ],
      ),
    );
  }

  Widget _deleteButton() {
    if (isCreateForm) return const SizedBox.shrink();

    return IconButton(
      onPressed: () => deleteRecipe(),
      icon: const Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }

  Widget _nameWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 16.0,
        horizontal: 16.0,
      ),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getLabelWidget("Name"),
          GetX<RecipeDetailsViewModel>(builder: (snapshot) {
            if (!isCreateForm && snapshot.nameStream.value.isEmpty) {
              return const SizedBox.shrink();
            }

            return TextFormField(
              onChanged: (value) {
                snapshot.nameStream.value = value;
              },
              initialValue: snapshot.nameStream.value,
              decoration: const InputDecoration(
                hintText: "Please key in your recipe name here",
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: AppColors.primaryColor,
                    width: 1.0,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 1.0,
                  ),
                ),
              ),
            );
          }),
        ],
      ),
    );
  }

  Widget _recipeImageWidget() {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getLabelWidget("Photo"),
          SizedBox(
            width: screenWidth,
            height: screenWidth / 2,
            child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return GetX<RecipeDetailsViewModel>(builder: (snapshot) {
                  List<int> photoBytes = snapshot.photoBytesStream.value;
                  return Stack(
                    fit: StackFit.expand,
                    children: [
                      Positioned.fill(
                        child: InkWell(
                          onTap: () => snapshot.selectImage(),
                          child: photoBytes.isEmpty
                              ? CircleAvatar(
                                  backgroundColor: Colors.transparent,
                                  child: ClipOval(
                                    child: Image.asset(
                                      'assets/images/icons/add.png',
                                      fit: BoxFit.fill,
                                      width: 40.0,
                                      height: 40.0,
                                    ),
                                  ),
                                )
                              : Container(
                                  color: Colors.transparent,
                                  child: Image.memory(
                                    Uint8List.fromList(photoBytes),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                        ),
                      )
                    ],
                  );
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _recipeTypeWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 16.0,
        horizontal: 16.0,
      ),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getLabelWidget("Recipe Type"),
          GetX<RecipeDetailsViewModel>(
            builder: (snapshot) {
              String recipeType = snapshot.recipeTypeStream.value;
              return DropdownButton(
                isExpanded: true,
                hint: const Text("Select recipe type"),
                value: recipeType.isEmpty ? null : recipeType,
                items: snapshot.recipeTypeListStream.map((value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (newValue) {
                  snapshot.updateRecipeType(newValue ?? "");
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _ingredientsWidget() {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      elevation: 4.0,
      margin: const EdgeInsets.all(0),
      child: Container(
        width: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 16.0,
              ),
              child: const Text(
                "Ingredients",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            IngredientListWidget(
              onIngredientAdded: (IngredientModel ingredient) {
                _viewModel.addIngredient(ingredient);
              },
              onIngredientUpdated: (int index, IngredientModel ingredient) {
                _viewModel.updateIngredient(index, ingredient);
              },
              onIngredientRemoved: (int index, IngredientModel ingredient) {
                _viewModel.removeIngredient(index);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _instructionWidget() {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      elevation: 4.0,
      margin: const EdgeInsets.all(0),
      child: Container(
        width: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 16.0,
              ),
              child: const Text(
                "Instructions",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            InstructionListWidget(
              onInstructionAdded: (InstructionModel instruction) {
                _viewModel.addInstruction(instruction);
              },
              onInstructionUpdated: (int index, InstructionModel instruction) {
                _viewModel.updateInstruction(index, instruction);
              },
              onInstructionRemoved: (int index, InstructionModel instruction) {
                _viewModel.removeInstruction(index);
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _getLabelWidget(String title) {
    return Container(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Text(
        title,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _submitButton() {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
          backgroundColor: Colors.black,
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 16,
          ),
        ),
        onPressed: () => submit(),
        child: Text(
          isCreateForm ? "Create Now" : "Update",
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  void deleteRecipe() {
    _viewModel.delete(widget.index!).then((value) {
      // Show error message if failed to delete
      if (value.status == Status.ERROR) {
        GetXHelper.showErrorMessage(value.message!);
      }

      if (value.status == Status.SUCCESS) {
        // Back to previous screen if delete successfully
        Get.back();
      }
    });
  }

  void submit() {
    _viewModel.submit(index: widget.index).then((value) {
      // Show error message if input fields are invalid or failed to persist
      if (value.status == Status.ERROR) {
        GetXHelper.showErrorMessage(value.message!);
      }

      if (value.status == Status.SUCCESS) {
        // Back to previous screen if submit successfully
        Get.back();
      }
    });
  }
}
