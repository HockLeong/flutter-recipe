import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_recipe/ui/recipe_details/recipe_details_screen.dart';
import 'package:flutter_recipe/vm/recipe_view_model.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../model/recipe_model.dart';

class RecipeScreen extends StatefulWidget {
  const RecipeScreen({Key? key}) : super(key: key);

  @override
  State<RecipeScreen> createState() => _RecipeScreenState();
}

class _RecipeScreenState extends State<RecipeScreen> {
  late RecipeViewModel _viewModel;

  @override
  void initState() {
    super.initState();

    // Instantiate view model
    _viewModel = Get.put(RecipeViewModel());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _viewModel.init();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Recipes",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          GetX<RecipeViewModel>(builder: (snapshot) {
            List<String> recipeTypes = snapshot.recipeTypeListStream.value;
            return PopupMenuButton<String>(
              child: const Icon(
                Icons.filter_alt_rounded,
                color: Colors.white,
              ),
              itemBuilder: (context) {
                return [
                  ...recipeTypes.map((e) {
                    return PopupMenuItem<String>(
                      child: Text(e),
                      onTap: () => snapshot.filter(e),
                    );
                  }).toList(),
                ];
              },
            );
          }),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: GetX<RecipeViewModel>(
              builder: (snapshot) {
                List<RecipeModel> recipeList = snapshot.isFiltered.value
                    ? snapshot.filteredRecipeListStream.value
                    : snapshot.recipeListStream.value;

                if (recipeList.isEmpty) {
                  return _emptyWidget();
                }

                return _recipeListWidget(recipeList);
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrangeAccent,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => _goToDetailsScreen(),
      ),
    );
  }

  Widget _recipeListWidget(List<RecipeModel> items) {
    return ListView.builder(
      itemCount: items.length,
      padding: const EdgeInsets.only(
        top: 8.0,
        left: 16.0,
        right: 16.0,
        bottom: 16.0,
      ),
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, int index) {
        RecipeModel item = items[index];

        int originalIndex = _viewModel.recipeListStream.indexOf(item);
        return Card(
          color: Colors.white,
          elevation: 4.0,
          margin: const EdgeInsets.only(top: 16.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: InkWell(
            onTap: () => _goToDetailsScreen(index: originalIndex),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: SizedBox(
                        width: double.infinity,
                        height: 150.0,
                        child: Image.memory(
                          Uint8List.fromList(item.photoBytes ?? []),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        item.name ?? "",
                        style: const TextStyle(
                          color: AppColors.primaryDarkTextColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _emptyWidget() {
    return const Center(
      child: Text('No result found'),
    );
  }

  void _goToDetailsScreen({int? index}) {
    Get.to(() => RecipeDetailsScreen(index: index));
  }
}
