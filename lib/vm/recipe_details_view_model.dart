import 'package:flutter_recipe/vm/recipe_view_model.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../helpers/asset_helper.dart';
import '../model/base_model.dart';
import '../model/constant_model.dart';
import '../model/ingredient_model.dart';
import '../model/instruction_model.dart';
import '../model/recipe_model.dart';
import '../repositories/recipe_repository.dart';

class RecipeDetailsViewModel extends GetxController {
  final RecipeRepository _recipeRepository = RecipeRepository();

  RxList<ConstantModel> unitListStream = <ConstantModel>[].obs;
  RxList<String> recipeTypeListStream = <String>[].obs;

  RxString nameStream = "".obs;
  RxString recipeTypeStream = "".obs;
  RxList<int> photoBytesStream = <int>[].obs;
  RxList<IngredientModel> ingredientModelListStream = <IngredientModel>[].obs;
  RxList<InstructionModel> instructionModelListStream =
      <InstructionModel>[].obs;

  Future<void> init({
    int? index,
  }) async {
    List<ConstantModel> unitList = await AssetHelper.getUnitList();
    unitListStream.assignAll(unitList);

    List<String> recipeTypeList = await AssetHelper.getRecipeTypeList();
    recipeTypeListStream.assignAll(recipeTypeList);

    if (index != null) {
      RecipeModel? data = await getRecipeByIndex(index);

      // Assign recipe data if not null
      if (data != null) {
        nameStream.value = data.name ?? "";
        recipeTypeStream.value = data.recipeType ?? "";
        photoBytesStream.value = data.photoBytes ?? [];
        ingredientModelListStream.value = data.ingredients ?? [];
        instructionModelListStream.value = data.instructions ?? [];
      }
    }
  }

  Future<RecipeModel?> getRecipeByIndex(int index) async {
    return await _recipeRepository.get(index);
  }

  Future<void> selectImage() async {
    try {
      final image = await ImagePicker().getImage(source: ImageSource.gallery);
      if (image == null) return;
      photoBytesStream.value = await image.readAsBytes();
    } catch (e) {
      print('Failed to pick image: $e');
    }
  }

  void updateRecipeType(String recipeType) {
    recipeTypeStream.value = recipeType;
  }

  void addIngredient(IngredientModel ingredientModel) {
    ingredientModelListStream.add(ingredientModel);
  }

  void updateIngredient(int position, IngredientModel ingredientModel) {
    ingredientModelListStream[position] = ingredientModel;
  }

  void removeIngredient(int position) {
    ingredientModelListStream.removeAt(position);
  }

  void addInstruction(InstructionModel instructionModel) {
    instructionModelListStream.add(instructionModel);
  }

  void updateInstruction(int position, InstructionModel instructionModel) {
    instructionModelListStream[position] = instructionModel;
  }

  void removeInstruction(int position) {
    instructionModelListStream.removeAt(position);
  }

  Future<BaseModel> delete(int index) async {
    try {
      RecipeViewModel recipeViewModel = Get.find();
      await _recipeRepository.delete(index);
      recipeViewModel.recipeListStream.removeAt(index);

      // Reset filter option and display all data once delete successfully
      recipeViewModel.isFiltered.value = false;
      return BaseModel.success();
    } catch (e) {
      return BaseModel.error(message: e.toString());
    }
  }

  Future<BaseModel> submit({
    int? index,
  }) async {
    bool isCreateForm = index == null;

    RecipeModel recipe = RecipeModel().copyWith(
      name: nameStream.value,
      recipeType: recipeTypeStream.value,
      photoBytes: photoBytesStream.value,
      ingredients: ingredientModelListStream.value,
      instructions: instructionModelListStream.value,
    );

    if (nameStream.isEmpty) {
      return BaseModel.error(message: "Name is required");
    }

    if (recipeTypeStream.isEmpty) {
      return BaseModel.error(message: "Recipe type is required");
    }

    if (photoBytesStream.isEmpty) {
      return BaseModel.error(message: "Please select recipe photo");
    }

    if (ingredientModelListStream.isEmpty) {
      return BaseModel.error(message: "Minimum ONE ingredient is required");
    }

    if (instructionModelListStream.isEmpty) {
      return BaseModel.error(message: "Minimum ONE instruction is required");
    }

    RecipeViewModel recipeViewModel = Get.find();

    try {
      if (isCreateForm) {
        await _recipeRepository.create(data: recipe);
        recipeViewModel.recipeListStream.add(recipe);
      } else {
        await _recipeRepository.update(index: index!, data: recipe);
        recipeViewModel.recipeListStream[index] = recipe;
      }

      // Reset filter option and display all data create/update successfully
      recipeViewModel.isFiltered.value = false;
      return BaseModel.success();
    } catch (e) {
      return BaseModel.error(message: e.toString());
    }
  }
}
