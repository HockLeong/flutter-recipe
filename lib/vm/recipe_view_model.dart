import 'package:flutter_recipe/helpers/asset_helper.dart';
import 'package:flutter_recipe/model/recipe_model.dart';
import 'package:flutter_recipe/repositories/recipe_repository.dart';
import 'package:get/get.dart';

class RecipeViewModel extends GetxController {
  final RecipeRepository _recipeRepository = RecipeRepository();
  RxList<RecipeModel> recipeListStream = <RecipeModel>[].obs;
  RxList<RecipeModel> filteredRecipeListStream = <RecipeModel>[].obs;
  RxList<String> recipeTypeListStream = <String>[].obs;
  RxBool isFiltered = false.obs;

  void init() async {
    List<String> recipeTypeList = await AssetHelper.getRecipeTypeList();
    recipeTypeListStream.assignAll(recipeTypeList);
    recipeTypeListStream.add("None");

    await getRecipeList();
  }

  Future<void> getRecipeList() async {
    List<RecipeModel> items = await _recipeRepository.getAll();
    recipeListStream.addAll(items);
  }

  Future<void> filter(String recipeType) async {
    if (recipeType == "None") {
      isFiltered.value = false;
      return;
    }

    List<RecipeModel> filterRecipeList = recipeListStream
        .where((item) => item.recipeType == recipeType)
        .toList();

    filteredRecipeListStream.assignAll(filterRecipeList);
    isFiltered.value = true;
  }
}
