import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_recipe/widgets/theme_text_field_widget.dart';
import 'package:get/get.dart';

import '../constants/colors.dart';
import '../helpers/getx_helper.dart';
import '../model/constant_model.dart';
import '../model/ingredient_model.dart';

class RecipeIngredientWidget extends StatefulWidget {
  final List<ConstantModel>? _constantModelList;
  final IngredientModel? ingredientModel;
  final Function(IngredientModel ingredientWidget)? onCallBack;

  const RecipeIngredientWidget(
    this._constantModelList, {
    super.key,
    this.ingredientModel,
    this.onCallBack,
  });

  @override
  State<RecipeIngredientWidget> createState() => _RecipeIngredientWidgetState();
}

class _RecipeIngredientWidgetState extends State<RecipeIngredientWidget> {
  final TextEditingController _ingredientNameController =
      TextEditingController();
  final TextEditingController _ingredientCountController =
      TextEditingController();
  int _selectedUnit = 0;

  @override
  void initState() {
    super.initState();
    if (widget.ingredientModel != null) {
      _selectedUnit = widget._constantModelList!.indexWhere(
          (element) => element.value == widget.ingredientModel!.unit);
      _ingredientNameController.text = widget.ingredientModel!.name!;
      _ingredientCountController.text =
          widget.ingredientModel!.count!.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Card(
            margin: const EdgeInsets.all(0),
            elevation: 4,
            child: ThemeTextFieldWidget(
              textController: _ingredientNameController,
              enabled: true,
              fillColor: AppColors.themeLightGray,
              borderRadius: BorderRadius.circular(4.0),
              hintText: "Ingredient",
              onChanged: (value) {
                setState(() {});
              },
              maxLength: 1000,
              padding: const EdgeInsets.only(bottom: 8.0),
              counterText: '${_ingredientNameController.text.length} / 1000',
            ),
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Flexible(
                flex: 5,
                child: SizedBox(
                  height: 100.0,
                  child: NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (scroll) {
                      scroll.disallowGlow();
                      return true;
                    },
                    child:
                        _getUnitListWheelScrollView(widget._constantModelList),
                  ),
                ),
              ),
              const SizedBox(width: 16.0),
              Flexible(
                flex: 5,
                child: Card(
                  margin: const EdgeInsets.all(0),
                  elevation: 4,
                  child: ThemeTextFieldWidget(
                    textController: _ingredientCountController,
                    textInputType:
                        const TextInputType.numberWithOptions(decimal: true),
                    textInputFormatterList: [
                      FilteringTextInputFormatter.allow(
                        RegExp(r'^(?!\.)(?:[0-9]{0,})(?:\.[0-9]{0,2})?'),
                      ),
                    ],
                    fillColor: AppColors.themeLightGray,
                    borderRadius: BorderRadius.circular(4.0),
                    hintText: '0',
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: const EdgeInsets.only(top: 32.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.0),
                  ),
                ),
                backgroundColor: AppColors.primaryColor,
                padding: const EdgeInsets.symmetric(
                  horizontal: 32.0,
                  vertical: 16.0,
                ),
              ),
              onPressed: () {
                String name = _ingredientNameController.text.trim();
                ConstantModel constantModel =
                    widget._constantModelList![_selectedUnit];
                String unitDescription = constantModel.value!;
                String ingredientCount = _ingredientCountController.text.trim();

                if (name.isEmpty) {
                  GetXHelper.showErrorMessage("Invalid ingredient name");
                  return;
                }

                if (ingredientCount.isEmpty) {
                  GetXHelper.showErrorMessage("Quantity cannot be empty");
                  return;
                }

                double count = double.parse(ingredientCount);

                widget.onCallBack?.call(
                  IngredientModel(
                    name: name,
                    unit: unitDescription,
                    count: count,
                  ),
                );

                Get.back();
              },
              child: Text(
                widget.ingredientModel == null ? "ADD" : "UPDATE",
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getUnitListWheelScrollView(List<ConstantModel>? constantModelList) {
    return ListWheelScrollView(
      controller: FixedExtentScrollController(initialItem: _selectedUnit),
      physics: const FixedExtentScrollPhysics(),
      itemExtent: 50.0,
      useMagnifier: true,
      magnification: 1.5,
      diameterRatio: 3.0,
      perspective: 0.01,
      squeeze: 1,
      onSelectedItemChanged: (index) {
        setState(() {
          _selectedUnit = index;
        });
      },
      children: constantModelList!.map((e) {
        int index = constantModelList.indexOf(e);
        return _getUnitItemWidget(index, e.description);
      }).toList(),
    );
  }

  Widget _getUnitItemWidget(int index, String? description) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      child: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.symmetric(vertical: 8.0),
        padding: const EdgeInsets.all(8.0),
        child: Text(
          description!,
          style: TextStyle(
            fontWeight:
                _selectedUnit == index ? FontWeight.bold : FontWeight.normal,
            color: _selectedUnit == index ? Colors.white : Colors.grey,
          ),
        ),
      ),
    );
  }
}
