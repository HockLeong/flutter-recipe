import 'package:flutter/material.dart';
import 'package:flutter_recipe/widgets/theme_text_field_widget.dart';
import 'package:get/get.dart';

import '../constants/colors.dart';
import '../helpers/getx_helper.dart';
import '../model/instruction_model.dart';

class RecipeInstructionWidget extends StatefulWidget {
  final Function(InstructionModel instructionModel)? onCallback;
  final InstructionModel? instructionModel;

  const RecipeInstructionWidget({
    super.key,
    this.instructionModel,
    this.onCallback,
  });

  @override
  State<StatefulWidget> createState() => _RecipeInstructionWidgetState();
}

class _RecipeInstructionWidgetState extends State<RecipeInstructionWidget> {
  final TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _descriptionController.text = widget.instructionModel!.description ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${'Step'} ${widget.instructionModel!.step}',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 16.0),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.2,
                child: Card(
                  elevation: 4.0,
                  margin: const EdgeInsets.all(0),
                  child: ThemeTextFieldWidget(
                    textController: _descriptionController,
                    textInputType: TextInputType.multiline,
                    hintText: "Description",
                    maxLines: 99,
                    onChanged: (value) {
                      setState(() {});
                    },
                    maxLength: 1000,
                    padding: const EdgeInsets.only(bottom: 8.0),
                    counterText: '${_descriptionController.text.length} / 1000',
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 32.0),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            backgroundColor: AppColors.primaryColor,
            padding: const EdgeInsets.symmetric(
              horizontal: 32.0,
              vertical: 16.0,
            ),
          ),
          onPressed: () async {
            final String description = _descriptionController.text.trim();

            if (description.isEmpty) {
              GetXHelper.showErrorMessage("Please fill in description");
              return;
            }

            widget.onCallback?.call(widget.instructionModel!.copyWith(
              description: description,
            ));

            Get.back();
          },
          child: Text(
            widget.instructionModel?.description == null ? "ADD" : "UPDATE",
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
