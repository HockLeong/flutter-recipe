import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_recipe/constants/colors.dart';

enum BoxDecorationType { none, round_corner }

class ThemeTextFieldWidget extends StatelessWidget {
  final bool enabled;
  final Widget? prefixIcon;
  final String? hintText;
  final String? errorText;
  final String? counterText;
  final bool isObscure;
  final TextInputType? textInputType;
  final TextCapitalization? textCapitalization;
  final TextEditingController? textController;
  final EdgeInsets padding;
  final EdgeInsets? contentPadding;
  final double fontSize;
  final Color fontColor;
  final Color hintColor;
  final Color iconColor;
  final Color borderColor;
  final Color fillColor;
  final FocusNode? focusNode;
  final ValueChanged? onFieldSubmitted;
  final ValueChanged? onChanged;
  final bool autoFocus;
  final bool isLabelTextAnimated;
  final TextInputAction? textInputAction;
  final BoxDecorationType boxDecorationType;
  final BorderRadius? borderRadius;
  final Widget? suffixIcon;
  final int? minLines;
  final int? maxLines;
  final int? maxLength;
  final bool isDense;
  final FontWeight fontWeight;
  final List<TextInputFormatter>? textInputFormatterList;
  final String? Function(dynamic)? validator;
  final TextStyle? textStyle;
  final TextAlign textAlign;

  const ThemeTextFieldWidget(
      {Key? key,
      this.enabled = true,
      this.prefixIcon,
      this.hintText,
      this.errorText,
      this.counterText = '',
      this.isObscure = false,
      this.textInputType,
      this.textCapitalization,
      this.textController,
      this.padding = EdgeInsets.zero,
      this.contentPadding,
      this.fontSize = 14,
      this.fontColor = Colors.black87,
      this.hintColor = Colors.grey,
      this.iconColor = Colors.grey,
      this.borderColor = Colors.transparent,
      this.fillColor = Colors.white,
      this.focusNode,
      this.onFieldSubmitted,
      this.onChanged,
      this.autoFocus = false,
      this.isLabelTextAnimated = false,
      this.textInputAction,
      this.boxDecorationType = BoxDecorationType.round_corner,
      this.borderRadius,
      this.suffixIcon,
      this.minLines,
      this.maxLines = 1,
      this.maxLength,
      this.isDense = true,
      this.fontWeight = FontWeight.normal,
      this.textInputFormatterList,
      this.validator,
      this.textStyle,
      this.textAlign = TextAlign.start})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle textFieldTextStyle = textStyle ??
        TextStyle(
          fontSize: fontSize,
          color: fontColor,
          fontWeight: fontWeight,
        );

    return Padding(
      padding: padding,
      child: TextFormField(
        enabled: enabled,
        maxLength: maxLength,
        controller: textController,
        focusNode: focusNode,
        minLines: minLines,
        maxLines: maxLines,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChanged,
        autofocus: autoFocus,
        textInputAction: textInputAction,
        obscureText: isObscure,
        // maxLength: 25,
        keyboardType: textInputType,
        textCapitalization: textCapitalization ?? TextCapitalization.sentences,
        style: textFieldTextStyle,
        textAlign: textAlign,
        inputFormatters: textInputFormatterList ?? [],
        validator: validator,
        decoration: InputDecoration(
          suffixIconConstraints: const BoxConstraints(
            maxHeight: 40,
            maxWidth: 40,
          ),
          contentPadding: contentPadding ?? const EdgeInsets.all(14.0),
          border: _getInputBorder(),
          isDense: isDense,
          enabledBorder: _getInputBorder(),
          focusedBorder: _getInputBorder(),
          labelText: isLabelTextAnimated ? hintText : null,
          labelStyle: textFieldTextStyle.copyWith(
              color: fontColor, fontWeight: fontWeight),
          hintText: hintText,
          hintStyle: textFieldTextStyle.copyWith(
            color: hintColor,
          ),
          prefixIcon: prefixIcon,
          filled: true,
          fillColor: enabled ? fillColor : AppColors.primaryLightTextColor,
          counterStyle: const TextStyle(
            height: 0,
            fontSize: 12,
            color: AppColors.primaryLightTextColor,
          ),
          counterText: counterText,
          suffixIcon: enabled && textController != null ? suffixIcon : null,
          errorText: errorText,
        ),
      ),
    );
  }

  InputBorder? _getInputBorder() {
    return boxDecorationType == BoxDecorationType.none
        ? const OutlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide.none,
          )
        : OutlineInputBorder(
            borderRadius: borderRadius ?? BorderRadius.circular(30.0),
            borderSide: BorderSide(
              color: borderColor,
            ),
          );
  }
}
