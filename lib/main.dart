import 'package:flutter/material.dart';
import 'package:flutter_recipe/model/instruction_model.dart';
import 'package:flutter_recipe/routes/app_router.dart';
import 'package:flutter_recipe/ui/recipe/recipe_screen.dart';
import 'package:flutter_recipe/ui/recipe_details/recipe_details_screen.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'constants/constant.dart';
import 'constants/themes.dart';
import 'model/ingredient_model.dart';
import 'model/recipe_model.dart';

void main() async {
  await _initHive();

  runApp(
    GetMaterialApp(
      title: 'Recipe',
      getPages: AppRouter.routes,
      theme: AppTheme.buildLightTheme(),
      home: const RecipeScreen(),
    ),
  );
}

Future<void> _initHive() async {
  try {
    await Hive.initFlutter();

    // Registering the adapter
    Hive.registerAdapter(RecipeModelAdapter());
    Hive.registerAdapter(IngredientModelAdapter());
    Hive.registerAdapter(InstructionModelAdapter());

    // Open respective hive box
    await Hive.openBox<RecipeModel>(Constant.RECIPE_BOX_NAME);
    await Hive.openBox<IngredientModel>(Constant.INGREDIENT_BOX_NAME);
    await Hive.openBox<IngredientModel>(Constant.INSTRUCTION_BOX_NAME);
  } catch (e) {
    debugPrint('Hive init Failed: $e');
  }
}
