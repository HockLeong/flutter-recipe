import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ingredient_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 1)
class IngredientModel {
  @HiveField(0)
  final double? count;

  @HiveField(1)
  final String? unit;

  @HiveField(2)
  final String? name;

  IngredientModel({
    this.count,
    this.unit,
    this.name,
  });

  IngredientModel copyWith({
    final double? count,
    final String? unit,
    final String? name,
  }) {
    return IngredientModel(
      count: count ?? this.count,
      unit: unit ?? this.unit,
      name: name ?? this.name,
    );
  }

  factory IngredientModel.fromJson(Map<String, dynamic> json) =>
      _$IngredientModelFromJson(json);

  Map<String, dynamic> toJson() => _$IngredientModelToJson(this);
}
