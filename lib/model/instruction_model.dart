import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instruction_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
class InstructionModel {
  @HiveField(0)
  final int? step;

  @HiveField(1)
  final String? description;

  InstructionModel({
    this.step,
    this.description,
  });

  InstructionModel copyWith({
    @HiveField(0) final int? step,
    @HiveField(1) final String? description,
  }) {
    return InstructionModel(
      step: step ?? this.step,
      description: description ?? this.description,
    );
  }

  factory InstructionModel.fromJson(Map<String, dynamic> json) =>
      _$InstructionModelFromJson(json);

  Map<String, dynamic> toJson() => _$InstructionModelToJson(this);
}
