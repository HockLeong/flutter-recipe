import 'package:json_annotation/json_annotation.dart';

part 'constant_model.g.dart';

@JsonSerializable()
class ConstantModel {
  final String? key;
  final dynamic value;
  final String? description;

  ConstantModel({
    this.key,
    this.value,
    this.description,
  });

  factory ConstantModel.fromJson(Map<String, dynamic> json) => _$ConstantModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConstantModelToJson(this);
}
