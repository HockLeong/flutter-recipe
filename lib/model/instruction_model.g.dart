// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instruction_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InstructionModelAdapter extends TypeAdapter<InstructionModel> {
  @override
  final int typeId = 2;

  @override
  InstructionModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InstructionModel(
      step: fields[0] as int?,
      description: fields[1] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, InstructionModel obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.step)
      ..writeByte(1)
      ..write(obj.description);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InstructionModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstructionModel _$InstructionModelFromJson(Map<String, dynamic> json) =>
    InstructionModel(
      step: json['step'] as int?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$InstructionModelToJson(InstructionModel instance) =>
    <String, dynamic>{
      'step': instance.step,
      'description': instance.description,
    };
