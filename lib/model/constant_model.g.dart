// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'constant_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConstantModel _$ConstantModelFromJson(Map<String, dynamic> json) =>
    ConstantModel(
      key: json['key'] as String?,
      value: json['value'],
      description: json['description'] as String?,
    );

Map<String, dynamic> _$ConstantModelToJson(ConstantModel instance) =>
    <String, dynamic>{
      'key': instance.key,
      'value': instance.value,
      'description': instance.description,
    };
