import 'package:flutter/foundation.dart';
import 'package:flutter_recipe/model/instruction_model.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import 'ingredient_model.dart';

part 'recipe_model.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class RecipeModel {
  @HiveField(0)
  final String? name;

  @HiveField(1)
  final String? recipeType;

  @HiveField(2)
  final List<IngredientModel>? ingredients;

  @HiveField(3)
  final List<InstructionModel>? instructions;

  @HiveField(4)
  final List<int>? photoBytes;

  RecipeModel({
    this.name,
    this.recipeType,
    this.ingredients,
    this.instructions,
    this.photoBytes,
  });

  RecipeModel copyWith({
    @HiveField(0) final String? name,
    @HiveField(1) final String? recipeType,
    @HiveField(2) final List<IngredientModel>? ingredients,
    @HiveField(3) final List<InstructionModel>? instructions,
    @HiveField(4) final List<int>? photoBytes,
  }) {
    return RecipeModel(
      name: name ?? this.name,
      recipeType: recipeType ?? this.recipeType,
      ingredients: ingredients ?? this.ingredients,
      instructions: instructions ?? this.instructions,
      photoBytes: photoBytes ?? this.photoBytes,
    );
  }

  factory RecipeModel.fromJson(Map<String, dynamic> json) =>
      _$RecipeModelFromJson(json);

  Map<String, dynamic> toJson() => _$RecipeModelToJson(this);
}
