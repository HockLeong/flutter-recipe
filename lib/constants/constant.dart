class Constant {
  static const String RECIPE_BOX_NAME = "recipeBox";
  static const String INGREDIENT_BOX_NAME = "ingredientBox";
  static const String INSTRUCTION_BOX_NAME = "instructionBox";
}
