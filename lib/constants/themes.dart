import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'colors.dart';

class AppTheme {
  static ThemeData buildLightTheme() {
    final ThemeData themeData = ThemeData.light();
    return themeData.copyWith(
      appBarTheme: const AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      colorScheme: AppColors.colorScheme,
      primaryColor: AppColors.primaryColor,
      indicatorColor: Colors.white,
      splashFactory: InkRipple.splashFactory,
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: AppColors.backgroundColor,
      hintColor: Colors.black12,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      primaryTextTheme: ThemeData.light().textTheme.apply(
            bodyColor: Colors.white,
          ),
      // accentTextTheme: ThemeData.light().textTheme,
    );
  }
}
