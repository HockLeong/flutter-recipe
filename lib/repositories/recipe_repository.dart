
import 'package:hive_flutter/hive_flutter.dart';

import '../constants/constant.dart';
import '../model/recipe_model.dart';

class RecipeRepository {
  late Box<RecipeModel> box;

  Future<Box<RecipeModel>> openRecipeBox() async {
    return await Hive.openBox<RecipeModel>(Constant.RECIPE_BOX_NAME);
  }

  Future<List<RecipeModel>> getAll() async {
    box = await openRecipeBox();
    return box.values.toList();
  }

  Future<RecipeModel?> get(int index) async {
    box = await openRecipeBox();
    return box.getAt(index);
  }

  Future<void> create({
    required RecipeModel data,
  }) async {
    box = await openRecipeBox();
    box.add(data);
  }

  Future<void> update({
    required int index,
    required RecipeModel data,
  }) async {
    box = await openRecipeBox();
    box.putAt(index, data);
  }

  Future<void> delete(int index) async {
    box = await openRecipeBox();
    box.deleteAt(index);
  }
}
