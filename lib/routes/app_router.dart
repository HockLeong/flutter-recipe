import 'package:flutter_recipe/ui/recipe_details/recipe_details_screen.dart';
import 'package:flutter_recipe/ui/recipe/recipe_screen.dart';
import 'package:get/get.dart';

class AppRouter {
  static final routes = [
    GetPage(
      name: '/recipes',
      page: () => const RecipeScreen(),
      children: [
        GetPage(
          name: '/details',
          page: () => const RecipeDetailsScreen(),
        ),
      ],
    ),
  ];
}
