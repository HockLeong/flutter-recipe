# Recipe App
A simple recipe list demo app written in Flutter using GetX. It is designed using MVVM architecture design pattern.
All data will be saved using Hive.

## Feature
- Recipe List (Page that display all recipe list, allow navigate to Recipe Details page)
- Recipe Details (Page that allows user create/edit/delete recipe item and display the details of the selected item)

## Getting Started

**Step 1:**
Clone the repo with the link below by using git:
```
git clone https://gitlab.com/HockLeong/flutter-recipe.git
```

**Step 2:**
Current project is using flutter **Version 3.3.6**. Ensure using the appropriate flutter version before running.

**Step 3:**
Go to project root and execute the following command in console to get the required dependencies:
```
flutter pub get 
```

### Libraries & Tools Used
- [x] [GetX](https://github.com/jonataslaw/getx)
- [X] [MVVM](https://developer.android.com/jetpack/guide)
- [x] [Hive](https://github.com/hivedb/hive/tree/master/hive)
- [x] [Hive Flutter](https://github.com/hivedb/hive/tree/master/hive_flutter)
- [x] [Image Picker](https://github.com/flutter/plugins/tree/main/packages/image_picker/image_picker)
- [X] [Json Annotation](https://github.com/google/json_serializable.dart/tree/master/json_annotation)

### Folder Structure
Here is the core folder structure flutter provides.
```
flutter-recipe/
|- android
|- build
|- ios
|- lib
|- linux
|- macos
|- test
|- web
|- windows
```

Here is the folder structure using in this project
```
lib/
|- constants/
|- helpers/
|- model/
|- repositories/
|- routes/
|- ui/
|- vm/
|- widgets/
|- main.dart
```

#### Constant
Contains constant required for the project like `colors`, `constant` and `themes`

#### Helpers
Contains the helper functions of your application

#### Model
Contains the data layer of your project
Model consists of all the data classes required for the project, including requests and responses

#### Repositories
Handle data operations and know where to get the data whether from local DB or API (For current project we used local DB only).
It acts as middleman between data sources

#### Routes
Contains all routes that app required for navigation purpose

#### UI
Contains all the layout required for the applications. E.g. screen

#### VM
Also known as ViewModel. VM basically handling all kinds of business logic and provides the data for a specific UI component

#### Widgets
Contain all common widgets that shared across multiple screens for the application

#### Main
Starting point of the application. All application level configurations are defined in this file
E.g. theme, routes, title, home screen and etc

## Unit Test
Here is the folder structure using for unit testing in this project
```
test
|- recipe_test.dart
```

Go to project root and execute the following command in console to run for unit testing:
```
flutter test test/recipe_test.dart
```
